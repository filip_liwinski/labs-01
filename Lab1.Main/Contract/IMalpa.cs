﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Implementation;

namespace Lab1.Contract
{
    public interface IMalpa
    {
        string Nakarm();

        int UkradnijBanana();
    }

}
