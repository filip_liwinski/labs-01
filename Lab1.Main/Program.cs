﻿using System;
using System.Collections.Generic;
using Lab1.Implementation;
using Lab1.Contract;

namespace Lab1.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Yeti yeti = new Yeti();
            yeti.Nakarm();
            ((IMalpa)yeti).Nakarm();
            ((IOlbrzym)yeti).Nakarm();
        }

        public IList<IMalpa> Pogrupuj()
        {
            IList<IMalpa> kolekcja = new List<IMalpa>{new Nizinny(), new Zachodni()};
            kolekcja.Add(new Zachodni());
            return kolekcja;
        }

        public int LiczMalpy(IList<IMalpa> kolekcja)
        {
            foreach (var item in kolekcja)
            {
                item.Nakarm();
            }
            return kolekcja.Count;
        }


    }
}
