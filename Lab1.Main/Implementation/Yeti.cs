﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class Yeti : IGoryl, IOlbrzym
    {

        string IOlbrzym.Nakarm()
        {
            return "Smaczne";
        }

        string IMalpa.Nakarm()
        {
            return "Niedobre";
        }

        public string Nakarm()
        {
            return "Okropne";
        }

        public bool Uciekaj()
        {
            return true;
        }

        public int UkradnijBanana()
        {
            return 8;
        }
    }
}
