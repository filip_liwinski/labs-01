﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IMalpa);
        
        public static Type ISub1 = typeof(IGoryl);
        public static Type Impl1 = typeof(Nizinny);
        
        public static Type ISub2 = typeof(IPawian);
        public static Type Impl2 = typeof(Zachodni);


        public static string baseMethod = "Nakarm";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "UkradnijBanana";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "UkradnijBanana";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Pogrupuj";
        public static string collectionConsumerMethod = "LiczMalpy";

        #endregion

        #region P3

        public static Type IOther = typeof(IOlbrzym);
        public static Type Impl3 = typeof(Yeti);

        public static string otherCommonMethod = "Nakarm";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
